import glfw
from OpenGL.GL import *
from OpenGL.GL.shaders import compileProgram, compileShader
import numpy as np

vertex_shader = """
#version 330

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec3 a_color;

out vec3 v_position;

void main()
{
    gl_Position = vec4(a_position, 0.0, 1.0);
    v_position = vec3(a_position, 0);
}
"""

# Волны
# fragment_shader = """
# #version 330

# in vec3 v_position;
# out vec4 out_color;

# void main()
# {
#     float x = v_position.x;
#     float y = v_position.y;
#     out_color = vec4(round(cos(y * 50 + sin(x * 50) * 5)), round(cos(y * 50 + sin(x * 50) * 5)), round(cos(y * 50 + sin(x * 50) * 5)), 1.0);
# }
# """

# Круги
fragment_shader = """
#version 330

in vec3 v_position;
out vec4 out_color;

void main()
{
    float x = v_position.x;
    float y = v_position.y;
    x = x * x;
    y = y * y;
    out_color = vec4(cos((x + y / 2.0) * 200), cos((x + y / 2.0) * 200), cos((x + y / 2.0) * 200), 1.0);
}
"""

if not glfw.init():
    raise Exception("glfw can't be inited")

window = glfw.create_window(720, 480, 'lab 1', None, None)

if not window:
    glfw.terminate()
    raise Exception("Can't create window")

glfw.set_window_pos(window, 100, 100)

glfw.make_context_current(window)


vertices = [
    -0.5, -0.5, 1.0, 0.0, 0.0,
    0.5, -0.5, 1.0, 0.0, 0.0,
    0.5,  0.5, 0.0, 0.0, 0.0,
    -0.5,  0.5, 0.0, 0.0, 0.0,
]

indices = [0, 1, 2, 2, 3, 0]

vertices = np.array(vertices, dtype=np.float32)

indices = np.array(indices, dtype=np.uint32)

shader = compileProgram(compileShader(vertex_shader, GL_VERTEX_SHADER), compileShader(fragment_shader, GL_FRAGMENT_SHADER))

VBO = glGenBuffers(1)  # указываем сколько буферов создаём
glBindBuffer(GL_ARRAY_BUFFER, VBO)  # биндим array buffer к объекту VBO (Vertex Buffer Object)
glBufferData(GL_ARRAY_BUFFER, vertices.nbytes, vertices, GL_STATIC_DRAW)  # непосредственно создаём буффер, указываем: какой буфер, сколько байт, объект с данными (копируется в буфер), режим

# То же самое проделываем с индексами, но используем не array buffer, а element array buffer
EBO = glGenBuffers(1)
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO)
glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.nbytes, indices, GL_STATIC_DRAW)

# делаем для вершин
glEnableVertexAttribArray(0)  # указываем индекс для нужного нам буфера
glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 20, ctypes.c_void_p(0))  # параметры: индекс, размер (число компонент, может быль 1-4), тип, нужно ли нормализовывать, сколько байт между 2 элементами, офсет для первого элемента

# то же самое для цвета
glEnableVertexAttribArray(1)
glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 20, ctypes.c_void_p(8))

glUseProgram(shader)
glClearColor(1, 1, 1, 1)

while not glfw.window_should_close(window):
    glfw.poll_events()
    
    glClear(GL_COLOR_BUFFER_BIT)
    
    glDrawElements(GL_TRIANGLES, len(indices), GL_UNSIGNED_INT, ctypes.c_void_p(0))
    
    glfw.swap_buffers(window)
    
glfw.terminate()